# AVL Trees

## Task
Implement AVL tree in selected language.

0. Create AVL class
1. Create AVLNode class and define its properties. What propertiest AVL node needs?
2. Create `empty()` function to check if AVL is empty or not
2. Create `push(Any)` function to add values to the AVL
3. Create `pop(Any)` function to remove values from the AVL
4. Create `find(Any)` function that will find value in AVL and return it