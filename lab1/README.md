# BST (binary search tree)


## Task
0. Create BST class
1. Create BSTNode class and define its properties. What propertiest BST node needs?
2. Create `empty()` function to check if BST is empty or not
2. Create `push(Any)` function to add values to the BST
3. Create `pop(Any)` function to remove values from the BST
4. Create `find(Any)` function that will find value in BST and return it
5. Create `min()` function that returns minimal value in BST
6. Create `max()` function that returns maximum value in BST

## Practical cases
Solve the problems related to the BST on [leetcode](https://leetcode.com). Some of them may be already solved in the previous part

0. [Searching in BST](https://leetcode.com/problems/search-in-a-binary-search-tree/)
1. [Increasing order](https://leetcode.com/problems/increasing-order-search-tree/)
2. [DFS](https://leetcode.com/problems/path-sum/)
4. [DFS](https://leetcode.com/problems/maximum-depth-of-n-ary-tree/)

To solve more problems related to the `BST` go [HERE](https://leetcode.com/tag/binary-search-tree/)
