class BSTNode():
    def __init__(self, key) -> None:
        self.left = None
        self.right = None
        self.key = key

    def push(self, key):
        if key == self.key:
            return
        elif key > self.key:
            if self.right == None:
                self.right = BSTNode(key)
            else:
                self.right.push(key)
        else:
            if self.left == None:
                self.left = BSTNode(key)
            else:
                self.left.push(key)
    
    def find(self, key) -> bool:
        if key == self.key:
            return True
        elif key > self.key:
            if self.right == None:
                return False
            else:
                return self.right.find(key)
        else:
            if self.left == None:
                return False
            else:
                return self.left.find(key)



class BST():
    def __init__(self):
        self.root = None

    def empty(self) -> bool:
        return self.empty == None

    def push(self, key):
        if self.root == None:
            self.root = BSTNode(key)
            return

        self.root.push(key)

    def find(self, key) -> bool:
        if self.empty():
            return False

        self.root.find(key)
        
