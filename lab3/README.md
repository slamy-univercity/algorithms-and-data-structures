# RB Trees

## Task
Implement RB tree in selected language.

0. Create RB class
1. Create RBNode class and define its properties. What propertiest RB node needs?
2. Create `empty()` function to check if RB is empty or not
2. Create `push(Any)` function to add values to the RB
3. Create `pop(Any)` function to remove values from the RB
4. Create `find(Any)` function that will find value in RB and return it